#ifndef SRC_INPUTS_CUSTOMELEMENT_HPP_
#define SRC_INPUTS_CUSTOMELEMENT_HPP_

#include "../Node.hpp"

class CustomElement: public Node {
public:
	CustomElement();
	virtual ~CustomElement();
};

#endif /* SRC_INPUTS_CUSTOMELEMENT_HPP_ */
