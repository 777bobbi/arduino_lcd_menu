useful references:
http://stackoverflow.com/questions/205945/why-does-the-c-stl-not-provide-any-tree-containers
http://codereview.stackexchange.com/questions/47395/tree-template-class-implementation-for-c
http://codereview.stackexchange.com/questions/9015/tree-node-class-deallocation-in-the-destructor?rq=1

* 1-st watering
 - Time
 - Duration
 - Frequency
* 2-nd watering
 - Time
 - Duration
* 3-rd watering
 - Time
 - Duration
- DateTime

Difference between menu and element:
Menu can contain new elements
Elements 
element we can add only to menu.

* menu 1
 |- el 1
 |- el 2
 |- el 3
-el 1
* menu 2
 |- el 1
 |- el 2
* menu 3
 |- el 1
 |- el 2
- el 2
* menu 4
 |- el
 * menu 1
  |- el 1
  |- el 2
 * menu 2
  |- el 1


// DEFINE ARDUINO PINS FOR THE NAVIGATION BUTTONS
#define UP_BUTTON_PIN       9
#define DOWN_BUTTON_PIN     10
#define LEFT_BUTTON_PIN     7
#define RIGHT_BUTTON_PIN    8
#define CONFIRM_BUTTON_PIN  12
#define ESCAPE_BUTTON_PIN   11

//rootMenu.draw();
//rootMenu.addControls();
//rootMenu.
// actionConfirmation(false)

//instantiate global variables to bind to menu
int      selectedOption = 0;
int      selectedInt = 0;
float    selectedFloat = 26.0;
boolean  selectedBoolean = 0;

int main () {
	MenuManager menu;

	//declare navigation buttons (required)
	menu.navButtons(UP_BUTTON_PIN,DOWN_BUTTON_PIN,ESCAPE_BUTTON_PIN,CONFIRM_BUTTON_PIN);

	//(optional)create a user define screen callback to activate after 10 secs (10.000 millis) since last button push
	menu.addUsrScreen(callback,10000);

	menu
	.addMenu("menu1")
		.addSelect("el1", selectedOption, callback) //create a variable of type "option list"; callback - optional.
			.addOption("option 1")
			.addOption("option 2")
			.addOption("option 3")
			.addOption("option 4")
		.done()
		.addInt("el2", selectedInt, callback)
		.addFloat("el3", selectedFloat, callback)
		.addTime("el4", callback)
		.addDateTime("el5", callback)
		.addBoolean("el6", selectedBoolean, callback)
		.addCustomElement("el7", callback)
	.done()

	.addCustomElement("el1", callback)

	.addMenu("menu2")
		.addCustomElement("el1", callback)
		.addCustomElement("el2", callback)
	.done()

	.addMenu("menu3")
		.addCustomElement("el1", callback)
		.addCustomElement("el2", callback)
	.done()

	.addCustomElement("el2", callback)

	.addMenu("menu4")
		.addCustomElement("el1", callback)

		.addMenu("menu1")
			.addCustomElement("el1", callback)
			.addCustomElement("el2", callback)
		.done()

		.addMenu("menu2")
			.addCustomElement("el1", callback)
			.addCustomElement("el2", callback)
		.done()
	.done();
}

